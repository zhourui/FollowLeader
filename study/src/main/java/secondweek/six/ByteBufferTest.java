package secondweek.six;

import org.nutz.lang.Encoding;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

/**
 * Created by zhourui on 2016/8/20.
 */
public class ByteBufferTest {

    public static void main(String[] args) throws Exception{
        File file = new File("D://aaaa.txt");
        FileChannel fileChannel = null;
        RandomAccessFile randomAccessFile = new RandomAccessFile(file,"rw");
        fileChannel = randomAccessFile.getChannel();
        int allocateLenght = 10;
        ByteBuffer byteBuffer = ByteBuffer.allocate(allocateLenght);
        int readLength = 0;
        while ((readLength = fileChannel.read(byteBuffer)) != -1){
            byteBuffer.flip();
            String eachRead = new String(byteBuffer.array(),0,readLength, Encoding.CHARSET_UTF8);
            System.out.println(eachRead);
            byteBuffer.clear();

        }
    }
}
