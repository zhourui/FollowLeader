package secondweek.four;

/**
 * Created by zhourui on 2016/8/12.
 */
public class SalaryTeam {
    private int from;
    private int to;
    private int total;

    public SalaryTeam(int from, int to, int total) {
        this.from = from;
        this.total = total;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "SalaryTeam{" +
                "from=" + from +
                ", to=" + to +
                ", total=" + total +
                '}';
    }
}
