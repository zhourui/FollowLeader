package secondweek.four;

/**
 * Created by zhour on 2016/8/19.
 */
public class SalaryTotal {
    private String twoWords;
    private int total;
    private int num;

    public SalaryTotal(String twoWords, int total, int num) {
        this.twoWords = twoWords;
        this.total = total;
        this.num = num;
    }

    public String getTwoWords() {
        return twoWords;
    }

    public void setTwoWords(String twoWords) {
        this.twoWords = twoWords;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
