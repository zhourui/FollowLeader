package secondweek.four;

/**
 * Created by zhourui on 2016/8/12.
 */
public class Salary {
    private String name;
    private int baseSalary;
    private int bonus;

    public Salary(String name, int baseSalary, int bonus) {
        this.name = name;
        this.bonus = bonus;
        this.baseSalary = baseSalary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(int baseSalary) {
        this.baseSalary = baseSalary;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return name + "," + baseSalary + "," + bonus;
    }
}
