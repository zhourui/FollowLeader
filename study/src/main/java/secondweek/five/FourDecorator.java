package secondweek.five;

import org.nutz.lang.Strings;

/**
 * 转成小写
 * Created by zhour on 2016/8/17.
 */
public class FourDecorator extends BaseDecorator {
    public FourDecorator(Component component) {
        super(component);
    }

    @Override
    public String dosomething() {
        String strIn = component.dosomething();
        if (Strings.isNotBlank(strIn)){
            return strIn.toLowerCase();
        }
        return "";
    }
}
