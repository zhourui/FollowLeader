package secondweek.five;

/**
 * Created by zhour on 2016/8/17.
 */
public class ConcreteComponent implements Component{
    private String systemIn;

    public ConcreteComponent(String systemIn){
        this.systemIn = systemIn;
    }

    public String dosomething() {
        return systemIn;
    }
}
