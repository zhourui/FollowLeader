package secondweek.five;

import org.nutz.lang.Strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by zhourui on 2016/8/11.
 * 用装饰者模式实现如下的功能：
 要求用户输入一段文字，比如 Hello Me，然后屏幕输出几个选项
 1 ：加密
 2 ：反转字符串
 3：转成大写
 4：转成小写
 5：扩展或者剪裁到10个字符，不足部分用！补充
 6:用户输入 任意组合，比如 1，3 表示先执行1的逻辑，再执行3的逻辑

 根据用户输入的选择，进行处理后，输出结果
 */
public class DecoratorTest {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = null;
        System.out.println("Enter a String:");
        str = br.readLine();
        System.out.println("1 ：加密");
        System.out.println("2 ：反转字符串");
        System.out.println("3：转成大写");
        System.out.println("4：转成小写");
        System.out.println("5：扩展或者剪裁到10个字符，不足部分用！补充");
        System.out.println("请输入任意组合，比如 1,3 表示先执行1的逻辑，再执行3的逻辑");
        String nums = br.readLine();

        Component component = new ConcreteComponent(str);
        if (Strings.isNotBlank(nums)){
            String[] numArray = nums.split(",");

            if (numArray.length > 0){
                for (String numStr : numArray){
                    int num = Integer.parseInt(numStr);
                    switch (num){
                        case 1 :
                            component = new OneDecorator(component);
                            break;
                        case 2 :
                            component = new TwoDecorator(component);
                            break;
                        case 3 :
                            component = new ThreeDecorator(component);
                            break;
                        case 4 :
                            component = new FourDecorator(component);
                            break;
                        case 5 :
                            component = new FiveDecorator(component);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        System.out.println("the result : " + component.dosomething());
    }
}
