package secondweek.five;

import org.nutz.lang.Strings;

/**
 * 转成大写
 * Created by zhour on 2016/8/17.
 */
public class ThreeDecorator extends BaseDecorator {
    public ThreeDecorator(Component component) {
        super(component);
    }

    @Override
    public String dosomething() {
        String strIn = component.dosomething();
        if (Strings.isNotBlank(strIn)){
            return strIn.toUpperCase();
        }
        return "";
    }
}
