package secondweek.five;

import org.nutz.lang.Lang;
import org.nutz.lang.Strings;

/**
 * 加密
 * Created by zhour on 2016/8/17.
 */
public class OneDecorator extends BaseDecorator {
    public OneDecorator(Component component) {
        super(component);
    }

    @Override
    public String dosomething() {
        String strIn = component.dosomething();
        if (Strings.isNotBlank(strIn)){
            return Lang.md5(strIn);
        }
        return "";
    }
}
