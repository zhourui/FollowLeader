package secondweek.five;

import org.nutz.lang.Strings;

/**
 * 扩展或者剪裁到10个字符，不足部分用！补充
 * Created by zhour on 2016/8/17.
 */
public class FiveDecorator extends BaseDecorator {
    public FiveDecorator(Component component) {
        super(component);
    }

    @Override
    public String dosomething() {
        String strIn = component.dosomething();
        if (Strings.isNotBlank(strIn)){
            return Strings.cutLeft(strIn,10,'!');
        }
        return "!!!!!!!!!!!";
    }
}
