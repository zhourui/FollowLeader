package secondweek.five;

/**
 * Created by zhour on 2016/8/17.
 */
public abstract class BaseDecorator implements Component {
    protected Component component;
    public BaseDecorator(Component component){
        this.component = component;
    }
}
