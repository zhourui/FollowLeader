package secondweek.five;

import org.nutz.lang.Strings;

/**
 * 反转字符串
 * Created by zhour on 2016/8/17.
 */
public class TwoDecorator extends BaseDecorator{

    public TwoDecorator(Component component) {
        super(component);
    }

    @Override
    public String dosomething() {
        String strIn = component.dosomething();
        return new StringBuilder(strIn).reverse().toString();
    }
}
