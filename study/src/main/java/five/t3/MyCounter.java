package five.t3;

/**
 * Created by zhour on 2016/9/9.
 */
public interface MyCounter{
    public void incr();
    public long getCurValue();//得到最后结果
}