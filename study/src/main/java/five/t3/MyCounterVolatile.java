package five.t3;

/**
 * Created by zhour on 2016/9/9.
 */
public class MyCounterVolatile implements MyCounter {
    private volatile long value;
    @Override
    public void incr() {
        value++;
    }

    @Override
    public long getCurValue() {
        return value;
    }
}
