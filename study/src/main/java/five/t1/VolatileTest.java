package five.t1;

import sun.misc.Unsafe;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 解释下你所理解的Happens-before含义和JVM里的几个Happens-before约定
 *  1、程序顺序规则：一个线程中的每个操作，happens- before 于该线程中的任意后续操作。
        （注解：如果只有一个线程的操作，那么前一个操作的结果肯定会对后续的操作可见。)
    2、监视器锁规则：对一个监视器锁的解锁，happens- before 于随后对这个监视器锁的加锁。
        （注解：这个最常见的就是syncronized 方法 和 syncronized块)
    3.volatile变量规则：对一个volatile域的写，happens- before 于任意后续对这个volatile域的读。
        int a =0;
        volatile int b = 0;
        线程1{
            操作1：a = 1;//插入一个StoreStore屏障 禁止上面的普通写与下面的volatile 写重排序
            操作2：b =2;
        }
        线程2{
            if (b==2)//LoadLoad屏障。 禁止上面的volatile 与下面的普通读重排序
            System.out.println(a);//a的值为多少呢？
        }
        因为存在屏障，JVM就不会重排序上述代码。
    4.传递性：如果A happens- before B，且B happens- before C，那么A happens- before C。
        （注解:这个看起来就像单线程顺序执行。。。）
 * Created by zhour on 2016/9/8.
 */
public class VolatileTest {
    private static volatile AtomicInteger a = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++){
            new Thread(()->{
                a.incrementAndGet();
                System.out.println(a);
            }).start();
        }

        Thread.sleep(100000);
    }

}
