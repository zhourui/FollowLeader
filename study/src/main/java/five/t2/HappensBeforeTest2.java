package five.t2;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by zhour on 2016/9/9.
 * 不依赖任何的同步机制（syncronized ,lock），有几种方式能实现多个线程共享变量之间的happens-before方式
 *
 * AbstractQueuedSynchronizer的release与acquire，setState与getState等等
 *
 * volatile
 * Unsafe.getUnsafe().compareAndSwap等
 * Unsafe.getUnsafe().fullFence();
 */
public class HappensBeforeTest2 {
    private static int aa = 0;
    private static int bb = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++){
            aa = 0;
            bb = 0;
            System.out.println("----------------------------------");
            new Thread(()->{
                aa++;
                bb++;
                UnSafeHelper.getUnsafeInstance().fullFence();
            }).start();

            new Thread(()->{
                System.out.println(aa+","+bb);
            }).start();
        }

        Thread.sleep(100000);
        //1、volatile
        AbstractQueuedSynchronizer abstractQueuedSynchronizer ;



        //2、Unsafe.getUnsafe().fullFence();


    }


}
