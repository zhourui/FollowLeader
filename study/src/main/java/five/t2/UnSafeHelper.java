package five.t2;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created by zhour on 2016/9/9.
 */
public class UnSafeHelper {
    //使用方法
    public static Unsafe getUnsafeInstance(){
        Field theUnsafeInstance = null;
        try {
            theUnsafeInstance = Unsafe.class.getDeclaredField("theUnsafe");

            theUnsafeInstance.setAccessible(true);
            return (Unsafe) theUnsafeInstance.get(Unsafe.class);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
