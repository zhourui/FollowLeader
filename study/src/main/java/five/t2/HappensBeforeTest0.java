package five.t2;

/**
 * Created by zhour on 2016/9/9.
 */
public class HappensBeforeTest0 {
    private static int aa = 0;
    private static int bb = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 100000; i++){
            aa = 0; bb = 0;
            System.out.println("----------------------------------");
            new Thread(()->{
                aa++;
                bb++;
            }).start();

            new Thread(()->{
                if(bb == 1)
                System.out.println(aa+","+bb);
            }).start();
            Thread.sleep(100);
        }

        Thread.sleep(100000);
    }
}
