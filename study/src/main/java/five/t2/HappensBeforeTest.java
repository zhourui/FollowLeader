package five.t2;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by zhour on 2016/9/9.
 * 不依赖任何的同步机制（syncronized ,lock），有几种方式能实现多个线程共享变量之间的happens-before方式
 *
 * AbstractQueuedSynchronizer的release与acquire，setState与getState等等
 *
 * volatile
 * Unsafe.getUnsafe().compareAndSwap等
 * Unsafe.getUnsafe().fullFence();
 */
public class HappensBeforeTest {
}
