package firstweek.three;

import java.util.Date;

/**
 * Created by zhourui on 2016/8/3.
 */
public class ByteArrayCal {
    public static void main(String[] args) {
        //准备数据
        byte[][] byteArray = new byte[10240][10240];
        for (int i = 0; i < 10240; i++){
            for (int j = 0; j < 10240; j++){
                byteArray[i][j] = 1;
            }
        }

        int totalNum = 0;
        //行优先
        Date dateStart = new Date();
        for (int i = 0; i < 10240; i++){
            for (int j = 0; j < 10240; j++){
                totalNum += byteArray[i][j];
            }
        }
        Date dateEnd = new Date();
        System.out.println("row first, cost time : "+ (dateEnd.getTime()-dateStart.getTime()) + "ms. totalNum is " + totalNum);

        totalNum = 0;
        //列优先
        dateStart = new Date();
        for (int i = 0; i < 10240; i++){
            for (int j = 0; j < 10240;j++){
                totalNum += byteArray[j][i];
            }
        }
        dateEnd = new Date();
        System.out.println("col first, cost time : "+ (dateEnd.getTime()-dateStart.getTime()) + "ms. totalNum is " + totalNum);
    }
}
