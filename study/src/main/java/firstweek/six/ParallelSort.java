package firstweek.six;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by zhourui on 2016/8/3.
 */
public class ParallelSort {

    @Test
    public void testParallelSort(){
        int length = 10000000;
        int[] a = new int[length];
        for (int i = 0; i < length; i++){
            a[i] = new Random().nextInt(Integer.MAX_VALUE);
        }
        Arrays.parallelSort(a);

        for (int i = length - 1; i >= length - 100; i--){
            System.out.println(a[i]);
        }
//        10000000000000;
        System.out.println(1 << 13);
        System.out.println(Integer.parseInt("10000000000000",2));
    }
}
