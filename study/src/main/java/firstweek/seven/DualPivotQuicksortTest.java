package firstweek.seven;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by zhour on 2016/8/4.
 */
public class DualPivotQuicksortTest {

    @Test
    public void testDualPivotQuicksort(){
        List<Integer> integers = new ArrayList<Integer>();
        Collections.sort(integers, new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
    }
}
