package firstweek.two;

/**
 * Created by zhour on 2016/8/2.
 */
public class IntShift {
    /**
     * int a=-1024;
     给出 a>>1与a>>>1的的结果，并且用位移方式图示解释
     * @param args
     */
    public static void main(String[] args) {
        int a=-1024;
        System.out.println(Integer.toBinaryString(a));
        //左侧补1
        System.out.println("a>>1 shift left end:-"+Integer.parseInt("10000000000000000000001000000000",2));

        System.out.println("a>>1:"+(a>>1));

        System.out.println("a>>>1 shift left :"+Integer.parseInt("01111111111111111111111000000000",2));
        System.out.println("a>>>1:"+(a>>>1));

        System.out.println(Integer.parseInt("1111111111111111111111111111111",2));
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);


        a=-2147483648;
        System.out.println(Integer.toBinaryString(a));
        System.out.println("1111111111111111111111111111111");
        System.out.println("a>>1:"+(a>>1));
        System.out.println("a>>>1:"+(a>>>1));

        System.out.println(Integer.parseInt("1111111111111111111111111111111",2));
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
    }
}
