package firstweek.five;

/**
 * Created by zhourui on 2016/8/3.
 */
public class MyItem {
    byte type;
    byte color;
    byte price;

    @Override
    public boolean equals(Object obj) {
        MyItem itemObj = ((MyItem) obj);
        return (type == itemObj.type) && (color == itemObj.color) && (price == itemObj.price);
    }

    @Override
    public String toString() {
        return "type:"+type+",color:"+color+",price:"+price;
    }
}
