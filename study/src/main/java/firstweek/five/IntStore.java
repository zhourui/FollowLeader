package firstweek.five;

/**
 * Created by zhour on 2016/8/4.
 */
public class IntStore {
    int[] storeByteArray = new int[1000];

    void putMyItem(int index, MyItem item){
        byte type = item.type;
        byte color = item.color;
        byte price = item.price;

        storeByteArray[index] =  type << 24 | color << 16 | price << 8 | 0;
    }

    MyItem getMyItem(int index){
        MyItem myItem = new MyItem();
        int value = storeByteArray[index];

        myItem.type = (byte) (value >> 24);
        //右移16位后将高位全部干掉
        myItem.color = (byte) ((value >> 16) & ((1<<8) -1));
        myItem.price = (byte) ((value >> 8) & ((1<<8) -1));
        return  myItem;
    }

    public static void main(String[] args) {
        System.out.println("1<<8-1 : " +(1<<8 - 1));
        System.out.println("(1<<8)-1 : " +((1<<8) - 1));
    }
}
