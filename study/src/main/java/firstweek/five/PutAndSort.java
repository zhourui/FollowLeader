package firstweek.five;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;
/**
 * Created by zhourui on 2016/8/3.
 */
public class PutAndSort {
    ByteStore byteStore;

    @Before
    public void before(){
        byteStore = new ByteStore();
    }
    @Test
    public void testPut() {
        MyItem item1 = new MyItem();
        item1.type = 1;
        item1.color = 2;
        item1.price = 3;
        byteStore.putMyItem(0,item1);

        MyItem item2 = new MyItem();
        item2.type = 4;
        item2.color = 5;
        item2.price = 6;
        byteStore.putMyItem(1,item2);

        MyItem item3 = new MyItem();
        item3.type = 7;
        item3.color = 8;
        item3.price = 9;
        byteStore.putMyItem(2,item3);

        MyItem getMtItem1 = byteStore.getMyItem(0);
        MyItem getMtItem2 = byteStore.getMyItem(1);
        MyItem getMtItem3 = byteStore.getMyItem(2);

        assertEquals(true, getMtItem1.equals(item1));
        assertEquals(true, getMtItem2.equals(item2));
        assertEquals(true, getMtItem3.equals(item3));
    }

    @Test
    public void testSort(){
        for (int i = 0; i < 1000; i++){
            MyItem item = new MyItem();
            byte[] bytes = new byte[3];
            new Random().nextBytes(bytes);
            item.type = bytes[0];
            item.color = bytes[1];
            item.price = bytes[2];
            byteStore.putMyItem(i,item);
        }

        //冒泡
        maopaosort(byteStore.storeByteArray,0);

        for (int i = 0; i < 100; i++){
            System.out.println(byteStore.getMyItem(999-i).toString());
        }
    }

    private void maopaosort(byte[] bytes, int finishSortNum){
        if (finishSortNum == bytes.length/3) return;
        byte type, color, price;

        for (int i = 0; i < (bytes.length/3 - finishSortNum - 1); i++){
            price = bytes[i*3+2];
            if (price > bytes[(i+1)*3+2]){
                type = bytes[(i+1)*3];
                color = bytes[(i+1)*3+1];
                price = bytes[(i+1)*3+2];

                bytes[(i+1)*3] = bytes[i*3];
                bytes[(i+1)*3+1] = bytes[i*3+1];
                bytes[(i+1)*3+2] = bytes[i*3+2];

                bytes[i*3] = type;
                bytes[i*3+1] = color;
                bytes[i*3+2] = price;
            }
        }

        finishSortNum++;
        maopaosort(bytes,finishSortNum);
    }
}
