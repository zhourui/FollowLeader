package firstweek.five;

/**
 * Created by zhourui on 2016/8/3.
 */
public class ByteStore {
    byte[] storeByteArray = new byte[3000];

    void putMyItem(int index, MyItem item){
        int arrayStartIndex = (index+1)*3-3;
        storeByteArray[arrayStartIndex] = item.type;
        storeByteArray[arrayStartIndex+1] = item.color;
        storeByteArray[arrayStartIndex+2] = item.price;
    }

    MyItem getMyItem(int index){
        MyItem myItem = new MyItem();
        int arrayStartIndex = (index+1)*3-3;
        myItem.type = storeByteArray[arrayStartIndex];
        myItem.color = storeByteArray[arrayStartIndex+1];
        myItem.price = storeByteArray[arrayStartIndex+2];
        return  myItem;
    }
}
