package firstweek.five;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by zhour on 2016/8/4.
 */
public class IntPutAndSort {
    IntStore intStore;

    @Before
    public void before(){
        intStore = new IntStore();
    }
    @Test
    public void testPut() {
        MyItem item1 = new MyItem();
        item1.type = 1;
        item1.color = 2;
        item1.price = 3;
        intStore.putMyItem(0,item1);

        MyItem item2 = new MyItem();
        item2.type = 4;
        item2.color = 5;
        item2.price = 6;
        intStore.putMyItem(1,item2);

        MyItem item3 = new MyItem();
        item3.type = 7;
        item3.color = 8;
        item3.price = 9;
        intStore.putMyItem(2,item3);

        MyItem getMtItem1 = intStore.getMyItem(0);
        MyItem getMtItem2 = intStore.getMyItem(1);
        MyItem getMtItem3 = intStore.getMyItem(2);

        assertEquals(true, getMtItem1.equals(item1));
        assertEquals(true, getMtItem2.equals(item2));
        assertEquals(true, getMtItem3.equals(item3));
    }

    @Test
    public void testSort(){
        for (int i = 0; i < 1000; i++){
            MyItem item = new MyItem();
            item.type = (byte) new Random().nextInt(128);
            item.color = (byte) new Random().nextInt(128);
            item.price = (byte) new Random().nextInt(128);
            intStore.putMyItem(i,item);
        }

        maopaosort(intStore.storeByteArray);

        for (int i = 0; i < 100; i++){
            System.out.println(intStore.getMyItem(999-i).toString());
        }
    }

    private void maopaosort(int[] bytes){
        int tempValue;
        for (int i = 0; i < bytes.length; i++){
            for (int j = 1; j < bytes.length-i; j++){
                if ( ((bytes[j-1] >> 8) & ((1<<8) -1)) > ((bytes[j] >> 8) & ((1<<8) -1)) ){
                    tempValue = bytes[j];
                    bytes[j] = bytes[j-1];
                    bytes[j-1] = tempValue;
                }
            }
        }
    }
}
