package firstweek.four;

import java.util.*;

/**
 * Created by zhourui on 2016/8/3.
 */
public class SortSalary {
    public static void main(String[] args) {
        List<Salary> salaries = generateSalaryList();
        Date dateStart = new Date();
        //begin to sort
        Collections.sort(salaries, new Comparator<Salary>() {
            public int compare(Salary o1, Salary o2) {
                return ((o2.baseSalary*13+o2.bonus)-(o1.baseSalary*13+o1.bonus));
            }
        });

        for (int i = 0; i < 10; i++){
            Salary salary = salaries.get(i);
            System.out.println(salary.name+" total:"+(salary.baseSalary*13+salary.bonus));
        }
        Date dateEnd = new Date();
        System.out.println("cost time : "+ (dateEnd.getTime()-dateStart.getTime())+"ms");

    }

    public static List<Salary> generateSalaryList(){
        List<Salary> salaries = new ArrayList<Salary>();
        for (int i = 0; i < 100000; i++){
            Salary salary = new Salary();
            salary.name = generateSixChar();
            salary.baseSalary = (new Random().nextInt(200000)+1) * 5;
            salary.bonus = new Random().nextInt(100001);
            salaries.add(salary);
        }
        return salaries;
    }

    private static String generateSixChar(){
        String a = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[] rands = new char[5];
        for (int i = 0; i < rands.length; i++)
        {
            int rand = (int) (Math.random() * a.length());
            rands[i] = a.charAt(rand);
        }
        return new String(rands);
    }
}
