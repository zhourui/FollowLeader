package firstweek.four;

import java.util.Date;
import java.util.List;

/**
 * Created by zhour on 2016/8/4.
 */
public class BubbleSort {
    public static void main(String[] args) {
        List<Salary> salaries = SortSalary.generateSalaryList();
        Salary[] salarieArray = salaries.toArray(new Salary[salaries.size()]);
        Date dateStart = new Date();
        bubbleSort(salarieArray);
        Date dateEnd = new Date();
        System.out.println("sort cost time : "+ (dateEnd.getTime()-dateStart.getTime())+"ms");
        System.out.println("max:"+(salarieArray[salarieArray.length-1].baseSalary*13
                +salarieArray[salarieArray.length-1].bonus));
        System.out.println("min:"+(salarieArray[0].baseSalary*13+salarieArray[0].bonus));
        for (int i = salarieArray.length-1; i >  salarieArray.length-1 - 100; i--){
            System.out.println(salarieArray[i].name+":"+(salarieArray[i].baseSalary*13
                    +salarieArray[i].bonus)/10000+"万");
        }
    }
    private static void bubbleSort(Salary[] salaries){
        int sort = 0;
        while (sort <= salaries.length -1){
            Salary salary;
            for (int i = 0; i < (salaries.length - sort - 1); i++){
                if (isBigger(salaries[i],salaries[i+1])){
                    salary = salaries[i];
                    salaries[i] =  salaries[i+1];
                    salaries[i+1] = salary;
                }
            }
            sort++;
        }
    }
    private static boolean isBigger(Salary salary1, Salary salary2){
        return ((salary1.baseSalary*13+salary1.bonus) > (salary2.baseSalary*13+salary2.bonus));
    }
}
