package firstweek.other;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.function.IntFunction;

/**
 * Created by zhourui on 2016/8/6.
 */
public class ArraysTest {

    @Test
    public void test(){
        final String[] aaArray = Arrays.asList("a","b","c").toArray(new String[3]);
        Assert.assertEquals(2,Arrays.binarySearch(aaArray,"c"));

        String[] bbArray = Arrays.copyOf(aaArray,10);
        Assert.assertEquals("a",bbArray[0]);
        Assert.assertEquals("b",bbArray[1]);
        Assert.assertEquals("c",bbArray[2]);
        Assert.assertEquals(null,bbArray[3]);
        Assert.assertEquals(10,bbArray.length);

        Arrays.setAll(aaArray, new IntFunction<String>() {
            public String apply(int value) {
                return "aa"+aaArray[value];
            }
        });

        System.out.println(Arrays.toString(aaArray));

    }
}
