package firstweek.other;

import static org.junit.Assert.*;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by zhourui on 2016/8/6.
 */
public class BigDecimalTest {

    @Test
    public void test() {
        BigDecimal bigDecimal1 = new BigDecimal(0.1);
        BigDecimal bigDecimal2 = new BigDecimal(0.10);

        assertEquals(true,bigDecimal1.equals(bigDecimal2));
        assertEquals(0,bigDecimal1.compareTo(bigDecimal2));

        BigDecimal bigDecimal3 = new BigDecimal("0.1");
        BigDecimal bigDecimal4 = new BigDecimal("0.10");
        assertEquals(false,bigDecimal3.equals(bigDecimal4));
        assertEquals(0,bigDecimal3.compareTo(bigDecimal4));

        BigDecimal nine = new BigDecimal("9");
        BigDecimal three = new BigDecimal("3");
        assertEquals(3,nine.divide(three).intValue());

        BigDecimal one = new BigDecimal("1");
        assertEquals(0,one.divide(three,2,BigDecimal.ROUND_UP).compareTo(new BigDecimal("0.34")));
        assertEquals(0,one.divide(three,2,BigDecimal.ROUND_DOWN).compareTo(new BigDecimal("0.33")));
    }
}
