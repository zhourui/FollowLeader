package firstweek.one;

/**
 * Created by zhour on 2016/8/2.
 */
public class ByteShift {
    /**
     *  byte ba=127;
        byte bb=ba<<2;
        System.out.println(bb);
        这个为什么会出错？给出解释，并且纠正错误
        答：ba<<2后值为508，byte最大为-128--127,不够
     * @param args
     */
    public static void main(String[] args) {
        byte ba = 127;
        int bb = ba<<2;
        System.out.println(bb);
        byte aa = Byte.parseByte("-127",10);
        System.out.println(aa);
    }
}
