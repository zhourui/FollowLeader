package three.t1;

import java.util.*;

/**
 * Created by zhour on 2016/8/24.
 * 分析Collection接口以及其子接口，很通俗的方式说说，究竟有哪些类型的Collection，各自解决什么样的问题
 *
 * Collection 抽象了一组元素的集合，最大化地提供了通用的集合操作方法。
 * 继承自Iterable接口，允许使用iterator方式遍历集合
 *
 * Collection子接口
 *      List 此接口可以对列表中每个元素的插入位置进行精确地控制。用户可以根据元素的整数索引（在列表中的位置）访问元素，并搜索列表中的元素。
 *      Set 一个不包含重复元素的 collection
 *      Queue 提供队列功能：FIFO
 *
 */
public class CollectionTest {

    public void test(){
        Collection<String> collection;

        List<String> list;

        Set<String> set;

        Queue<String> queue;
    }
}
