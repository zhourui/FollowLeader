package three.t5;

import org.junit.Test;

import java.util.*;

/**
 * Created by zhour on 2016/8/24.
 * Java数组如何与Collection相互转换
 */
public class ArrayAndCollectionTest {

    @Test
    public void test(){
        String[] strArray = {"1","2","3"};
        List<String> strList =  Arrays.asList(strArray);
        strArray = (String[]) strList.toArray();

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.toArray();

    }
}
