package three.t2;

import java.util.*;

/**
 * Created by zhour on 2016/8/24.
 * TreeSet继承了什么Set，与HashSet的区别是？HashSet与HashTable是“一脉相承”的么？
 *
 * TreeSet继承自AbstractSet，实现了NavigableSet接口,
 * 提供一个使用树结构存储Set接口的实现，对象以升序顺序存储，访问和遍历的时间很快.
 * TreeSet是通过TreeMap实现的.
 * HashSet继承AbstractSet，实现了Set接口，使用HashMap实现。
 *
 * HashSet与HashTable是“一脉相承”的么？
 * 不是，HashTable是键值对的形式，继承自Dictionary，实现了Map接口。实现和使用场景均不同。
 *
 */
public class TreeSetTest {

    public void test(){
        TreeSet<String> treeSet;
        HashSet<String> hashSet;

        Hashtable<String,String> hashtable;
        HashMap<String,String> hashMap;

        TreeMap<String,String> treeMap;
    }
}
