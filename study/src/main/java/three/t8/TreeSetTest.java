package three.t8;

import java.util.TreeSet;

/**
 * Created by zhour on 2016/8/24.
 * TreeSet里的自定义对象必须要实现什么方法，说明原因
 *
 * 自己自定义的类就必须要去实现Comparator接口，并且实现接口里compare方法，以此来确定比较的规则，然后TreeSet根据这个规则对里面的对象进行排序
 */
public class TreeSetTest {
    public void test(){
        TreeSet<String> treeSet = new TreeSet<>();
    }
}
