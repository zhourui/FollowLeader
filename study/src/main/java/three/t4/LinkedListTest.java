package three.t4;

import org.junit.Test;

import java.util.LinkedList;

/**
 * Created by zhour on 2016/8/24.
 * LinkedList也是一种Queue么？是否是双向链表?
 *
 * LinkedList也是一种queue.LinkedList实现接口Deque,而Deque继承自Queue
 *
 * LinkedList是双向链表。
 *
 *
 */
public class LinkedListTest {
    @Test
    public void test(){
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("a");
        linkedList.add("b");
        linkedList.add("c");
        linkedList.add("d");

        System.out.println(linkedList.get(6));
    }
}
