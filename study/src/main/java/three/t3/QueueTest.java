package three.t3;

import java.util.Queue;

/**
 * Created by zhour on 2016/8/24.
 * Queue接口增加了哪些方法，这些方法的作用和区别是？
 *
 * Queue接口增加的方法：add,offer,remove,poll,element,peek
 *
 * add,offer都是添加元素到队列中。
 * remove,poll都是从队列中移除元素，同时返回移除的元素.
 * add()和remove()方法在失败的时候会抛出异常，而offer()和poll()通过返回值可以判断成功与否。
 * 所以应尽量使用offer()和poll()
 *
 * element,peek都是查看队列Head中的元素，但是不从队列中移除。
 * element在队列为空时会抛出异常，peek在队列为空时会返回null
 *
 */
public class QueueTest {

    public void test(){
        Queue<String> queue;
    }
}
