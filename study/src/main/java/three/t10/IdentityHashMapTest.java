package three.t10;

import org.junit.Test;

import java.util.IdentityHashMap;
import java.util.stream.Stream;

/**
 * Created by zhour on 2016/8/24.
 * IdentityHashMap 里面如果按照下面的方法放入对象，分别是什么结果，请解释原因
 *   Integer a=5;
     Integer b=5;
     map.put(a,"100");
     map.put(b,"100";
     System.out.println(map.size);
     map.clear();
     Integer a=Integer.MAX_VALUE-1;
     Integer b=Integer.MAX_VALUE-1;
     map.put(a,"100");
     map.put(b,"100";
     System.out.println(map.size);

 IdentityHashMap是一个特殊的Map实现，它要求 两个key严格相等时才认为两个key相等
 */
public class IdentityHashMapTest {

    @Test
    public void test(){
        IdentityHashMap map = new IdentityHashMap();
        Integer a= 5;
        Integer b= 5;
        System.out.println("a:"+a+",b:"+b+" a==b:"+(a==b));
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());
        map.clear();
        a=Integer.MAX_VALUE-1;
        b=Integer.MAX_VALUE-1;
        System.out.println("a:"+a+",b:"+b+" a==b:"+(a==b));
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());

        for (int i = 0; i < 200; i++){
            a= i;
            b= i;
            System.out.println("Integer a="+a+",Integer b="+b+" a==b:"+(a==b));
        }
    }
}
